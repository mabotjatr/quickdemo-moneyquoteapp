package com.example.moneyquoteapp.model;

import lombok.Data;

@Data
public class UssdResponse {
    private String sessionId;
    private String message;
}
