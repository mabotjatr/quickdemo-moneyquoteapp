package com.example.moneyquoteapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MoneyquoteAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(MoneyquoteAppApplication.class, args);
    }

}
